export default class DataUtil{
    static ships={};
    static loadShipsData(useData){
        fetch('https://digitalshipapi.onrender.com/api/ships')
        .then(async response => {
            const data = await response.json();

            // check for error response
            if (!response.ok) {
                // get error message from body or default to response statusText
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            }
            DataUtil.ships=data;
            useData();
        })
        .catch(error => {
            console.error('There was an error!', error);
            DataUtil.ships= {};
            useData();
        });
    }  
}