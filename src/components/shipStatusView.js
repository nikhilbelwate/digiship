import {
  PieChart,
  Pie,
  Cell,
  Label,
  Text,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import { useState, useEffect } from "react";
import config from "../config.json";

const ShipStatusView = ({stats}) => {
  const [shipStatusData, setshipStatusData] = useState([]);
  //WS
  useEffect(() => {
    if (stats !== null && stats !== undefined) {
      console.log('stats in S_View', stats);
      setshipStatusData(stats);
    }
  }, [stats, setshipStatusData]);


  const COLORS = ["#C8102E", "#C8C9C7", "#001A71", "#5B7F95"];
//First time
  useEffect(() => {
    fetch(config.REACT_APP_SERVER_URL+"/api/shipstatus")
      .then((response) => response.json())
      .then((data) => setshipStatusData(data));
  }, []);
  return (
    <div className="">
      <span className="font-bold text-lg ml-2">Fleet Statistic View</span>
      <div className="grid xl:grid-cols-4 mb-4 ml-1 2xl:ml-4 md:grid-cols-3 sm:grid-cols-2">
        {shipStatusData.map((parameter, index) => (
          <div
            key={`cell-${index}`}
            // style={{ width: "96%", height: "24em" }}
            className="py-3 2xl:mx-2 rounded-lg bg-white shadow-2xl h-96 xl:w-52 2xl:w-72"
          >
            <ResponsiveContainer width="100%" height="60%">
              <PieChart width={1000} height={400} >
                 {/* className="2xl:translate-x-9" */}
                <Pie
                  data={parameter.data}
                  cx='50%'
                  cy={110}
                  innerRadius={48}
                  outerRadius={73}
                  fill="#8884d8"
                  dataKey="value"
                  label ={{}}
                >
                  {parameter.data.map((entry, index) => (
                    <Cell
                      key={`cell-${index}`}
                      fill={COLORS[index % COLORS.length]}
                    />
                  ))}
                </Pie>
                <Legend />
              </PieChart>
            </ResponsiveContainer>
            <h2 className="-z-1 text-center text-md font-medium ml-24 pl-4 2xl:ml-32 2xl:pl-8 -translate-x-1/2 -translate-y-1/2" 
            style={{
              marginTop:'3.7em',
              zIndex: -1,
              width: 'min-content',
              lineHeight:'1.2em',
              height:'inherit'}}>{parameter.name}</h2>
          </div>
        ))}
      </div>
    </div>
  );
};

export default ShipStatusView;


