import React from "react";
import boat from "../images/seaspan_tugboat.svg";


const ShipOverView = (props) => {
  return (
    <div>
      <div
        style={{
          //   backgroundColor: "#D0F0C0",
          backgroundColor: `${props.bgcolor}`,
          height: "180px",
          width: "600px",
          paddingLeft: "0.75em",
          paddingTop: "0.1em",
          marginBottom: "1em",
        }}
      >
        <h3
          style={{
            // backgroundColor: "#1e9054",
            backgroundColor: `${props.textbgcolor}`,
            width: "fit-content",
            color: "white",
            padding: "0.4em",
            marginTop: "0.5em",
            borderRadius: "0.5em",
          }}
        >
          {props.name}
        </h3>
        <div className="flex">
          <div>
          <img
            className="m-2 mt-4"
            src={boat}
            alt="Atlantic Raven"
            width="100"
            height="100"
          />
          <h className="ml-10 font-semibold">Revan</h>
          </div>
          
          <div>
          <img
            className="m-2 mt-4"
            src={boat}
            alt="Atlantic Eagle"
            width="100"
            height="100"
          />
          <h className="ml-10 font-semibold">Eagle</h>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShipOverView;