import React, { useState, useEffect } from "react";
import Boat from "../components/Boat/Boat";
import BoatIcon from "../images/seaspan_tugboat.svg";
import JSSIcon from "../images/Seaspan_JSS.svg";
import OFSVIcon from "../images/Seaspan_OFSV.svg";
import OOSVIcon from "../images/Seaspan_OOSV.svg";
import DataUtil from "../reducers/DataUtil";

const OperationStatusView = ({ship_prop}) => {
  const [ships, setShipsData] = useState([]);
  useEffect(() => {
    if (ship_prop !== null && ship_prop !== undefined) {
      console.log('ships in OPS', ship_prop);
      setShipsData(ship_prop);
    }
  }, [ship_prop]);


  const [isOpen, setIsOpen] = useState(false);

  const handlePopup = () => {
    setIsOpen((prevOpen) => !prevOpen);
  };

  // const useData = () => {
  //   setShips(DataUtil.ships);
  // };
  // DataUtil.loadShipsData(useData);
  
  const boatList = [
    {
      title: "Tugboats",
      ships: [{name:"Ocean Tugs", icon:BoatIcon},{name:"Coastal Tugs", icon:BoatIcon},{name:"Assist & Escort", icon:BoatIcon},{name:"River Tugs", icon:BoatIcon}],
    },
    { title: "Royal Canadian Navy", ships: [{name:"JSS", icon:JSSIcon}]},
    { title: "Canadian Coast Guard", ships: [{name:"OFSV", icon:OFSVIcon},{name:"OOSV", icon:OOSVIcon},{name:"MPV", icon:BoatIcon},{name:"Polar", icon:BoatIcon}] },
  ];

  const shipOverviewList = [
    {
      name: "Underway",
      status: "Running",
      ships: [],
    },
    {
      name: "Maintenance",
      status: "Main",
      ships: [],
    },
    {
      name: "Idle - Crew On",
      status: "Idle",
      ships: [],
    },
    {
      name: "Idle - Crew Off",
      status: "Off",
      ships: [],
    },
  ];
  if(ships!==null && ships!==undefined){
  Object.entries(ships).map(([k, s]) => {
    console.log('s',s);
    if (s.status.type === "Running") shipOverviewList[0].ships.push(s);
    else if (s.status.type === "Main") shipOverviewList[1].ships.push(s);
    else if (s.status.type === "Idle") shipOverviewList[2].ships.push(s);
    else shipOverviewList[3].ships.push(s);
  });
}
  return (
    <>
      <div className="flex">
        <h2 className="font-bold text-lg ml-2">Operation Summary View</h2>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 24 24"
          fill="currentColor"
          onClick={handlePopup}
          className="w-6 h-6 ml-4 pt-2"
        >
          <path
            fill-rule="evenodd"
            d="M3.792 2.938A49.069 49.069 0 0112 2.25c2.797 0 5.54.236 8.209.688a1.857 1.857 0 011.541 1.836v1.044a3 3 0 01-.879 2.121l-6.182 6.182a1.5 1.5 0 00-.439 1.061v2.927a3 3 0 01-1.658 2.684l-1.757.878A.75.75 0 019.75 21v-5.818a1.5 1.5 0 00-.44-1.06L3.13 7.938a3 3 0 01-.879-2.121V4.774c0-.897.64-1.683 1.542-1.836z"
            clip-rule="evenodd"
          />
        </svg>
      </div>
      {isOpen && (
        <div className="opacity-90 z-40 ml-20 absolute rounded-xl">
          <div className="pl-6 w-72 bg-slate-100">
            <h2>Filter Vessel Class</h2>
            {boatList.map((obj) => (
              <>
                <li>
                  {obj.title}
                  <ul className="pl-6">
                    {obj.ships.map((boat) => (
                      <li className="flex">
                        <img src={boat.icon} alt="Boat icon" />
                        <span className="mt-3.5">{boat.name}</span>
                      </li>
                    ))}
                  </ul>
                </li>
              </>
            ))}
          </div>
        </div>
      )}

      {shipOverviewList.map((item, index) => (
        <div
          key={`tile-${index}`}
          style={{
            height: "54%",
            width: "100%",
            display: "flex",
            paddingLeft: "0.75em",
            paddingTop: "0.1em",
            marginBottom: "20px",
          }}
        >
          <div className="h-full w-full bg-white rounded-xl shadow-xl">
            <span className="p-2 font-semibold">
              {item.name}
            </span>
            <div className="m-1.5 pl-1.5"
              style={{
                float: "right",
                backgroundColor: "black",
                borderRadius: "50%",
                width: "1.5em",
                height: "1.5em",
                display: "flex-inline",
                justifyContent: "center",
                alignItems: "center",
                color: "white",
              }}
            >
              {item.ships.length}
            </div>
            
            <div
              style={{
                display: "grid",
                overflow: "hidden",
                gridTemplateColumns: "80px 80px 80px",
                gap: "5px",
              }}
            >
              {item.ships.map((s, i) => (
                <Boat key={s.id} ship={s} />
              ))}
            </div>
          </div>
        </div>
      ))}
    </>
  );
};
export default OperationStatusView;
