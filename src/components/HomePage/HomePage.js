import React, { useState, useEffect } from "react";
import "./HomePage.module.css";
import MapView from "../MapView/MapView";
import OperationStatusView from "../operationStatusView";
import ShipStatusView from "../shipStatusView";
import useWebSocket, { ReadyState } from 'react-use-websocket';
import config from "../../config.json";

const HomePage = () => {
  const [message, setMessage] = useState({});
  

  const { lastJsonMessage, readyState } = useWebSocket(config.REACT_APP_SOCKET_URL,{
    onOpen: () => {
      console.log('WebSocket connection established.');
    },
    onClose: (e)=>{
      console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
    },
    shouldReconnect: (closeEvent) => true
  });
  useEffect(() => {
    if (lastJsonMessage !== null) {
      console.log('last message', lastJsonMessage);
      setMessage(lastJsonMessage);
    }
  }, [lastJsonMessage, setMessage]);

    //console.log('home page message', message);

  return (
    <React.Fragment>
      <div className="grid grid-cols-12 gap-2 mx-auto mt-14">
        <div className="col-span-8 row-span-3 m-2">
          <MapView width="65.5%" height="346px" />
        </div>
        <div className="col-span-4 row-span-7 m-2">
          <OperationStatusView ship_prop={message.ships}/>
        </div>
        <div className="col-span-8 row-span-4 mt-6">
          <ShipStatusView stats={message.stats} />
        </div>
      </div>
    </React.Fragment>
  );
};

HomePage.propTypes = {};

HomePage.defaultProps = {};

export default HomePage;
