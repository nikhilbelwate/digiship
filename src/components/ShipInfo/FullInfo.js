import { useParams } from 'react-router-dom'
import { useState, useEffect } from "react";
import ModelInfo from "./ModelInfo";
export default function FullInfo({ sId=0 }) {
    const [ship, updateShip]=useState({});
    const { shipId } = useParams();
    useEffect(() => {
        let url="";
        if(sId!==0)
            url="https://digitalshipapi.onrender.com/api/ships/"+sId;
        else
            url="https://digitalshipapi.onrender.com/api/ships/"+shipId;
        fetch(url)
          .then((response) => response.json())
          .then((data) => updateShip(data));
      }, [shipId,sId]);
    return (<div>
        <ModelInfo ship={ship}/>
    </div>
    )

}