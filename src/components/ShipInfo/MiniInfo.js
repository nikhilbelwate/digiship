import { Link } from "react-router-dom";
export default function MiniInfo({ ship, isFromMap = false }) {
  const url = "/ship/" + ship.id;
  return (
    <div className="w-max ml-3">
      <h1
        className="w-max text-lg font-bold p-1 border-b-4 border-green-800"
        style={{ "text-decoration-color": "green" }}
      >
        {ship.name}
      </h1>
      <p className="text-sm mt-6">
        N {ship.currentLocation[0].toFixed(6)} W{" "}
        {ship.currentLocation[1].toFixed(6)}
      </p>
        {/* ob/Status
            Average Speed – knots
            Course - direction
            Destination/ETA – 24 hour clock
            Fuel Burn (L/h) */}
      <table className="table-auto content-center border-separate border-spacing-2 border-slate-400">
        <tbody>
        <tr className="border border-slate-300">
            <td>Status</td>
            <th>Running</th>
            <td></td>
          </tr>
          <tr className="border border-slate-300">
            <td>Avg Speed</td>
            <th>{ship.avgSpeed.toFixed(1)}</th>
            <td> knots </td>
          </tr>
          <tr className="border border-slate-300">
            <td>Course</td>
            <th>North</th>
            <td></td>
          </tr>
          <tr className="border border-slate-300">
            <td>Destination/ETA</td>
            <th>N/A</th>
            <td></td>
          </tr>
          <tr className="border border-slate-300">
            <td>Fuel Burn</td>
            <th>40</th>
            <td> L/h </td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
