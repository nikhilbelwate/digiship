import { Link } from 'react-router-dom';
import { Card } from '@material-tailwind/react';
export default function ModelInfo({ ship }) {

    return <Card className='h-auto rounded z-40 shadow'>
        <h1 className="absolute text-lg font-bold top-0 left-0 p-1 underline decoration-4" style={{ 'text-decoration-color': 'green' }}>
            {ship.name}
        </h1>

        <Link to="/" className="absolute z-40 right-1 p-1 top-0 mr-4">
            <svg width="15pt" height="15pt" version="1.1" viewBox="0 0 700 700" xmlns="http://www.w3.org/2000/svg">
                <g>
                    <path d="m590.02 53.852c-2.1172-0.88281-4.3906-1.3438-6.6875-1.3516h-186.66c-6.2539 0-12.031 3.3359-15.156 8.75s-3.125 12.086 0 17.5 8.9023 8.75 15.156 8.75h144.42l-180.12 180.13h-0.003906c-3.3008 3.2773-5.1641 7.7344-5.1719 12.383-0.007812 4.6523 1.8359 9.1172 5.125 12.406 3.2891 3.2891 7.7539 5.1328 12.406 5.125 4.6523-0.007813 9.1094-1.8672 12.387-5.1719l180.12-180.12v144.42c0 6.25 3.3359 12.027 8.75 15.152 5.4141 3.1289 12.086 3.1289 17.5 0 5.4141-3.125 8.75-8.9023 8.75-15.152v-186.67c-0.011719-3.457-1.043-6.832-2.9688-9.707-1.9219-2.8711-4.6523-5.1133-7.8438-6.4414z" />
                    <path d="m166.07 528.1h321.2c17.738-0.023437 34.746-7.082 47.285-19.629 12.543-12.547 19.598-29.555 19.617-47.293v-123.53c0-6.25-3.3359-12.027-8.75-15.152-5.4141-3.1289-12.086-3.1289-17.5 0-5.4141 3.125-8.75 8.9023-8.75 15.152v123.53c-0.003907 8.4609-3.3672 16.574-9.3477 22.559-5.9805 5.9883-14.094 9.3555-22.555 9.3633h-321.2c-8.4609-0.007812-16.57-3.375-22.551-9.3633-5.9844-5.9844-9.3438-14.098-9.3477-22.559v-271.77c0.007812-8.4609 3.3711-16.566 9.3516-22.547s14.09-9.3477 22.547-9.3555h148.25c6.2539 0 12.031-3.3359 15.156-8.75s3.125-12.086 0-17.5-8.9023-8.75-15.156-8.75h-148.25c-17.734 0.019531-34.742 7.0742-47.285 19.617-12.539 12.543-19.594 29.547-19.613 47.285v271.77c0.015625 17.738 7.0703 34.746 19.613 47.293s29.547 19.605 47.285 19.629z" />
                </g>
            </svg>
        </Link>
        <button className="absolute z-40 right-0 p-1 top-0">
            <svg width="15pt" height="15pt" viewPort="0 0 12 12" version="1.1"
                xmlns="http://www.w3.org/2000/svg">
                <line x1="1" y1="11"
                    x2="11" y2="1"
                    stroke="black"
                    stroke-width="2" />
                <line x1="1" y1="1"
                    x2="11" y2="11"
                    stroke="black"
                    stroke-width="2" />
            </svg>
        </button>
        <br/>

        <p className="text-sm m-1 pt-2 pl-8">
       {ship.currentLocation} 
        </p>

        <table className="table-auto content-center border-separate border-spacing-2 border-slate-400">
      <tbody>
        <tr className='border border-slate-300'>
          <td>Avg Speed</td>
          <th >{ ship.avgSpeed}</th>
          <td> rpm </td>
        </tr>
        <tr className='border border-slate-300'>
          <td>Fuel Rate</td>
          <th>{ ship.fuleLavel}</th>
          <td> L/h </td>
        </tr>
        <tr className='border border-slate-300'>
          <td>Engine load</td>
          <th>40</th>
          <td> hp </td>
        </tr>
      </tbody>
    </table>
    </Card>;
}
