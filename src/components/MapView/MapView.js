import React from 'react';
import { MapContainer, TileLayer, LayersControl, Circle, LayerGroup } from 'react-leaflet';
import './MapView.module.css';
// import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import MarkersView from './MarkersView';


export default class MapView extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      ships: {},
      width: props.width,
      height: props.height,
      mapHeight: props.height
    };
  }
  componentDidMount() {
    this.updateMapSize();
    window.addEventListener('resize', this.updateMapSize);
  }

  componentWillUnmount() {
    window.removeEventListener('resize', this.updateMapSize);
  }

  updateMapSize = () => {
    const height = window.innerHeight - this.mapContainer.offsetTop;
    this.setState({ mapHeight: height });
  };
  handleMove = (event) => {
    console.log(event.target.getCenter()+ " # "+ event.target.getZoom());
  };
  render() {
    console.log("In Render MApView")
    let position = {
      lat: 49.328096,
      lng: -123.278771
    }
    let group1 = [49.40, -123.50];
    let group2 = [50.10, -123.75];
    let { BaseLayer } = LayersControl;

    let { width, height } = this.state;
    if (typeof width === 'undefined' || typeof height === 'undefined') {
      this.setState({ height: 'Calc(100% - 20px)' });
      this.setState({ width: 'Calc(100% - 30px)' });
    }
    return (
      <div className="bg-white h-full w-full rounded-lg shadow-2xl" ref={(el) => (this.mapContainer = el)}>
        <MapContainer center={position} zoom={12} scrollWheelZoom={true} onMove={this.handleMove} placeholder={<p>Map not loaded</p>} className="geoMapContainer m-0.5" style={{
            position: 'absolute',
            height: this.state.mapHeight,
            width: this.state.width,
            zIndex: 0,
            padding:'1px',
            backgroundColor:'white',
            radius:'2px',
            boxShadow:'initial'
          }}>
          <LayersControl position="topright">
            <BaseLayer checked name="Basic">
            <TileLayer
              url='https://tiles.stadiamaps.com/tiles/alidade_smooth/{z}/{x}/{y}{r}.png'
              maxZoom={20}
            />
          </BaseLayer>
          <BaseLayer name="Standard">
          <TileLayer
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
          </BaseLayer>
          <BaseLayer name='Satellite'>
            <TileLayer
              url='https://{s}.google.com/vt/lyrs=s&x={x}&y={y}&z={z}'
              maxZoom={20}
              subdomains={['mt1', 'mt2', 'mt3']}
            />
          </BaseLayer>
          <LayersControl.Overlay checked name="Markers">
            <MarkersView />
          </LayersControl.Overlay>
          <LayersControl.Overlay name="Group view">
            <LayerGroup>
              <Circle
                center={group1}
                pathOptions={{ fillColor: 'blue' }}
                radius={200}
              />
              <Circle
                center={group2}
                pathOptions={{ fillColor: 'red' }}
                radius={300}
                stroke={false}
              />
              <LayerGroup>
                <Circle
                  center={[51.51, -122.50]}
                  pathOptions={{ color: 'green', fillColor: 'green' }}
                  radius={400}
                />
              </LayerGroup>
            </LayerGroup>
          </LayersControl.Overlay>
          <LayersControl.Overlay name='SeaMap'>
          <TileLayer
            url='https://tiles.openseamap.org/seamark/{z}/{x}/{y}.png'
             // url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
          </LayersControl.Overlay>
        </LayersControl>
      </MapContainer>
      </div>
    );
  }
}
