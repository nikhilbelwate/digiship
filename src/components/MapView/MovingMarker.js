import { useEffect, useState } from "react";
import ReactLeafletDriftMarker from "react-leaflet-drift-marker";
import L from "leaflet";
import { Popup, Tooltip } from "react-leaflet";
import Boat from "../../images/seaspan_tugboat.svg";
import Running from "../../images/Seaspan_tugboat_dsiss.svg";
import Idle from "../../images/idea_boat.svg";
import Off from "../../images/maintance_boat.svg";

import MiniInfo from "../ShipInfo/MiniInfo";

export default function MovingMarker({ ship }) {
  const lat = ship.currentLocation[0];
  const lng = ship.currentLocation[1];
  const [prevPos, setPrevPos] = useState([lat, lng]);
  const icon = L.icon({
    iconSize: [70, 70],
    popupAnchor: [2, -20],
    iconUrl:
      ship.status.type === "Running"
        ? Running
        : ship.status.type === "Idle"
        ? Idle
        : ship.status.type === "Off"
        ? Off
        : Boat,
  });
  useEffect(() => {
    if (prevPos[1] !== lng && prevPos[0] !== lat) setPrevPos([lat, lng]);
  }, [lat, lng, prevPos]);

  return (
    <ReactLeafletDriftMarker
      icon={icon}
      position={[lat, lng]}
      previousPosition={prevPos}
      key={ship.id}
    >
      <Popup>
        <MiniInfo ship={ship} />
      </Popup>
      <Tooltip>
        <MiniInfo ship={ship} />
      </Tooltip>
    </ReactLeafletDriftMarker>
  );
}
