import { LayerGroup } from 'react-leaflet';
import './MapView.module.css';
import 'leaflet/dist/leaflet.css';
import React, { useState, useEffect }from "react";
import MovingMarker from './MovingMarker';
import useWebSocket, { ReadyState } from 'react-use-websocket';
import config from "../../config.json";

const MarkersView = () => {
    const [ships, setShips] = useState({});
    const { lastJsonMessage, readyState } = useWebSocket(config.REACT_APP_SOCKET_URL,{
        onOpen: () => {
            console.log('WebSocket connection established.');
          },
          onClose: (e)=>{
            console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
          },
          shouldReconnect: (closeEvent) => true
    });
    useEffect(() => {
        if (lastJsonMessage !== null) {
            setShips(lastJsonMessage.ships);
        }
    }, [lastJsonMessage, setShips]);

    /*console.log("In Render MarkersView");
      
    let cnt = 0;
    let forword = true;
    setTimeout(() => {// updates position every 5 sec
        if (cnt === 110) {
            forword = false;
        } else {
            if (cnt === 0) {
                forword = true;
            }
        }
        if (!forword) {
            cnt--;
            Object.entries(this.state.ships).forEach(([key, ship]) => {
                ship.currentLocation[0] = parseFloat(ship.currentLocation[0]) - 0.001;
                ship.currentLocation[1] = parseFloat(ship.currentLocation[1]) - 0.0001;
                //console.log(`${key}: [${ship.currentLocation[0]},${ship.currentLocation[1]}]`);
            });
        } else {
            Object.entries(this.state.ships).forEach(([key, ship]) => {
                ship.currentLocation[0] = parseFloat(ship.currentLocation[0]) + 0.001;
                ship.currentLocation[1] = parseFloat(ship.currentLocation[1]) + 0.0001;
                //console.log(`${key}: [${ship.currentLocation[0]},${ship.currentLocation[1]}]`);
            });
            cnt++;
        }
        this.setState({ ships: this.state.ships });
    }, 10000);*/
    return (<LayerGroup>
        {Object.entries(ships).map(([key, ship]) => {
            return <MovingMarker key={ship.id} ship={ship ?? {}} />;
        })}
    </LayerGroup>);

};

MarkersView.propTypes = {};

MarkersView.defaultProps = {};

export default MarkersView;
