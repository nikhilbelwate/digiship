//import logo from './logo.svg';
import './App.css';
import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import MapView from "./components/MapView/MapView";
import HomePage from "./components/HomePage/HomePage";
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import Boat from './components/Boat/Boat';
import OperationStatusView from './components/operationStatusView';
import FullInfo from './components/ShipInfo/FullInfo';

function App() {
  const demoship={name:'Raven', status:'Running', type:'tug boat'};
  return (
      <div className='App'>
        <Router className="bg-gray-100 min-h-screen">
          <Header/>
        <Routes>
          <Route exact path="/" element={<HomePage />} />
          <Route exact path="/ship/:shipId" target="framename" rel="noopener noreferrer" element={<FullInfo />}/>
          <Route exact path="/contact" element={<OperationStatusView />} />
          <Route exact path="/map" element={<React.Fragment><MapView width='50%' height='420px'/></React.Fragment>} />
        </Routes>
        </Router>
    <Footer />
    </div>
  );
}

export default App;
