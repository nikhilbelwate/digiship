# Introduction

This repository is the front end application for the Digital Ship In-service Support System (DISS).

# Local Environment Requirements

- Docker for Windows
- Node.js

# Technologies

The following technologies are used in the repository:

- Node.js
- React.js
- CSS 3
- HTML 5
- Tailwind

## Project Libraries
- [ ] Add Leaflet to Repo
- [ ] Add Tailwind to Repo
- [ ] Add Icon Libraries to Repo
- [ ] Add Recharts to Repo
 
# Helpful Links
- React Documentation: (https://beta.reactjs.org/)
- Docker Documentation:(https://docs.docker.com/)
- Recharts: (https://recharts.org/en-US)
- Leaflet: (https://react-leaflet.js.org/)